# FreeBSD overlay ports

This repo contains modified port of FreeBSD www/nginx-naxsi

Official port is not fully functional because of incompatibility of naxsi with prce2. In this port is pcre2 disabled.

[Here](https://bugs.freebsd.org/bugzilla/show_bug.cgi?id=271064) is bug report.

## Getting started

```
pkg install poudriere-devel
```

## Build modified nginx

Create new ports collection in poudriere:

```
poudriere ports -c -U https://gitlab.com/pvalenta/freebsd-overlay-ports.git -p pvalenta -m git -B main
```

To build nginx-naxsi:

```
poudriere bulk -j 13_2 -p local -O pvalenta -C 'www/nginx-naxsi'
```

To update ports collection use:

```
poudriere ports -p pvalenta -u
```
